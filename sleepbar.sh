#!/usr/bin/env bash
if [ "$(echo $1 | grep -o "^[0-9][0-9]*$")" == "" ]; then
    echo "usage: $0 seconds"
    echo "   eg: $0 100"
    exit 1
fi

for (( i=1; i<=$1; i=$i+1)); do
    percent=$(echo "$i/($1/100)"|bc -l|sed 's/\..*//')
    if [ "$percent" == "" ]; then
        percent=0
    fi
    echo -ne "\r"
    echo -ne "["
    for (( j=1; j<=$percent; j=$j+2 )); do
        echo -ne "="
    done
    for (( j=1; j<100-$percent; j=$j+2 )); do
        echo -ne "-"
    done
    echo -ne "] $percent% / "

    remaining_secs=$(($1-$i))
    printf ""[%dh:%dm:%ds]"" \
        $(($remaining_secs/3600)) \
        $(($remaining_secs%3600/60)) \
        $(($remaining_secs%60))
    sleep 1
done
echo
exit 0
