#!/usr/bin/env bash

# source this script in your script and use fancy colors
# eg:
#  . /usr/bin/wab/wab-colors.sh
#  echo -e "${C_RED}wooooooo${C_RESET}normal"

if [ "$(/usr/bin/env tput colors)" != 8 ]; then
    # foreground colors
    C_RED="\e[0;31m"
    C_BLACK="\e[0;30m"
    C_DARK_GRAY="\e[1;30m"
    C_BLUE="\e[0;34m"
    C_LIGHT_BLUE="\e[1;34m"
    C_GREEN="\e[0;32m"
    C_LIGHT_GREEN="\e[1;32m"
    C_CYAN="\e[0;36m"
    C_LIGHT_CYAN="\e[1;36m"
    C_RED="\e[0;31m"
    C_LIGHT_RED="\e[1;31m"
    C_PURPLE="\e[0;35m"
    C_LIGHT_PURPLE="\e[1;35m"
    C_BROWN="\e[0;33m"
    C_YELLOW="\e[1;33m"
    C_LIGHT_GRAY="\e[0;37m"
    C_WHITE="\e[1;37m"

    # background colors
    CB_RED="\e[0;41m"
    CB_BLACK="\e[0;40m"
    CB_DARK_GRAY="\e[1;40m"
    CB_BLUE="\e[0;44m"
    CB_LIGHT_BLUE="\e[1;44m"
    CB_GREEN="\e[0;42m"
    CB_LIGHT_GREEN="\e[1;42m"
    CB_CYAN="\e[0;46m"
    CB_LIGHT_CYAN="\e[1;46m"
    CB_RED="\e[0;41m"
    CB_LIGHT_RED="\e[1;41m"
    CB_PURPLE="\e[0;45m"
    CB_LIGHT_PURPLE="\e[1;45m"
    CB_BROWN="\e[0;43m"
    CB_YELLOW="\e[1;43m"
    CB_LIGHT_GRAY="\e[0;47m"
    CB_WHITE="\e[1;47m"

    C_RESET="\e[00m"
fi
